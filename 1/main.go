package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const (
	InputFile  = "input.txt"
	WindowSize = 3
)

func main() {
	measurements := make([]int, 0)
	measurements, err := parseInput(InputFile, measurements)
	if err != nil {
		log.Fatal(err)
	}

	increases := SlidingWindowIncreases(measurements, 1)
	fmt.Printf("Part 1: %v\n", increases)
	increases = SlidingWindowIncreases(measurements, WindowSize)
	fmt.Printf("Part 2: %v\n", increases)
}

func sum(data []int) int {
	sum := 0
	for _, d := range data {
		sum += d
	}
	return sum
}

func SlidingWindowIncreases(data []int, window int) int {
	increases := 0
	for i := 0; i < len(data)-window; i++ {
		cur := sum(data[i : i+window])
		next := sum(data[i+1 : i+window+1])
		if cur < next {
			increases++
		}
	}
	return increases
}

func parseInput(file string, buf []int) ([]int, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("could not parse input: %v", err)
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	for s.Scan() {
		var num int
		_, err := fmt.Sscan(s.Text(), &num)
		if err != nil {
			return nil, fmt.Errorf("could not parse input: %v", err)
		}
		buf = append(buf, num)
	}
	return buf, nil
}

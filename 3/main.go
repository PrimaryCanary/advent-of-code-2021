package main

import (
	"bufio"
	"fmt"
	"log"
	"math/bits"
	"os"
	"strconv"
)

const InputFile = "input.txt"

func ParseInput(file string) ([]uint64, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("could not parse input: %v", err)
	}
	defer f.Close()

	buf := make([]uint64, 0)
	s := bufio.NewScanner(f)
	for s.Scan() {
		num, err := strconv.ParseUint(s.Text(), 2, 32)
		if err != nil {
			return nil, fmt.Errorf("could not parse input: %v", err)
		}
		buf = append(buf, num)
	}

	return buf, nil
}

// CountOnesInPos pos is zero indexed i.e. 2^pos gives place value
func CountOnesInPos(ints []uint64, pos int) int {
	count := 0
	for _, x := range ints {
		count += (int(x) >> pos) & 1
	}

	return count
}

func MoreOnesInPos(ints []uint64, pos int) bool {
	return CountOnesInPos(ints, pos) > len(ints) / 2
}

func Max(a []uint64) uint64 {
	max := a[0]
	for _, x := range a {
		if x > max {
			max = x
		}
	}

	return max
}

func CalcEpsilon(ints []uint64, m int) int {
	ep := 0
	for i := 0; i < m; i++ {
		if MoreOnesInPos(ints, i) {
			ep += 1 << i
		}
	}

	return ep
}

// MaxBits number of bits needed to represent the maximum number given
func MaxBits(ints []uint64) int {
	return bits.Len64(Max(ints))
}


func main() {
	nums, err := ParseInput(InputFile)
	if err != nil {
		log.Fatalln(err)
	}
	mb := MaxBits(nums)

	epsilon := CalcEpsilon(nums, mb)
	gamma := (^epsilon) << (64 - mb) >> (64 - mb)
	fmt.Printf("%v * %v = %v\n", epsilon, gamma, epsilon * gamma)
}

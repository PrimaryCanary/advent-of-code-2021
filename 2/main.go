package main

import (
	"bufio"
	"fmt"
	"log"
	"os"

	"gitlab.com/PrimaryCanary/advent-of-code-2021/2/command"
)

type Position struct {
	x, y, aim int
}

func (p *Position) Move(c command.Command) {
	switch c.Dir {
	case command.Up:
		// p.y -= c.Magnitude
		p.aim -= c.Magnitude
	case command.Down:
		// p.y += c.Magnitude
		p.aim += c.Magnitude
	case command.Forward:
		p.x += c.Magnitude
		p.y += p.aim * c.Magnitude
	}
}

func ParseInput(file string) ([]command.Command, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("could not parse input: %w", err)
	}
	defer f.Close()

	buf := make([]command.Command, 0)
	s := bufio.NewScanner(f)
	for s.Scan() {
		c, err := command.Parse(s.Text())
		if err != nil {
			return nil, fmt.Errorf("could not parse input: %w", err)
		}
		buf = append(buf, c)
	}

	return buf, nil
}

const InputFile = "input.txt"

func main() {
	p := Position{0, 0, 0}
	cmds, err := ParseInput(InputFile)
	if err != nil {
		log.Fatalln(err)
	}

	for _, c := range cmds {
		p.Move(c)
	}
	fmt.Printf("position: %+v, multiplied: %v\n", p, p.x*p.y)
}

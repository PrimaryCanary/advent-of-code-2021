package command

import (
	"fmt"
	"strconv"
	"strings"
)

const (
	upText      = "up"
	downText    = "down"
	forwardText = "forward"
)

type Direction int

const (
	Up Direction = iota
	Down
	Forward
)

type Command struct {
	Dir       Direction
	Magnitude int
}

func Parse(cmd string) (Command, error) {
	split := strings.Split(cmd, " ")
	if len(split) != 2 {
		return Command{}, fmt.Errorf("could not parse command: too many items")
	}

	magnitude, err := strconv.Atoi(split[1])
	if err != nil {
		return Command{}, fmt.Errorf("could not parse command: %w", err)
	}

	d := Forward + 1
	switch split[0] {
	case upText:
		d = Up
	case downText:
		d = Down
	case forwardText:
		d = Forward
	}

	return New(d, magnitude)
}

func New(d Direction, magnitude int) (Command, error) {
	switch d {
	case Up, Down, Forward:
		return Command{d, magnitude}, nil
	default:
		return Command{}, fmt.Errorf("could not create command: unknown direction")
	}
}
